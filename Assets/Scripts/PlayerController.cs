﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	Rigidbody2D rBody;
	public float moveFactor = 10f;
	public float speed;
	private bool horizontal;


	// Use this for initialization
	void Start () {
		rBody = this.GetComponent<Rigidbody2D>();
		horizontal = true;
		speed = 30f;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown (KeyCode.Return))
		   Application.LoadLevel (Application.loadedLevel+1);
	}

	void FixedUpdate() {
		if (horizontal) {
			MovementHorizontal ();
			AddForceRight ();
		} else {
			MovementVertical();
			AddForceUp ();
		}
	}

	void MovementHorizontal()
	{
		//Refactor later
		float levitate = 4*Mathf.Sin (Time.time*10);
		float vertical = Input.GetAxis ("Vertical");
		if (vertical != 0)
			levitate = 0;
		levitate += -Physics2D.gravity.y;
		
		rBody.velocity = Vector2.zero;
		rBody.AddForce (new Vector2 (0, vertical * moveFactor));
		rBody.AddForce (new Vector2(0, levitate));
	}

	void AddForceRight()
	{
		rBody.AddForce (new Vector2(speed, 0));
	}

	void MovementVertical()
	{
		float levitate = 4*Mathf.Sin (Time.time*10);
		float horizontal = Input.GetAxis ("Horizontal");
		if (horizontal != 0)
			levitate = 0;
		levitate += -Physics2D.gravity.y;
		
		rBody.velocity = Vector2.zero;
		rBody.AddForce (new Vector2 (horizontal*moveFactor, 0));
		rBody.AddForce (new Vector2(0, levitate));
	}

	void AddForceUp()
	{
		rBody.AddForce (new Vector2 (0, speed));
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Gravity")
			horizontal = !horizontal;
	}
}
