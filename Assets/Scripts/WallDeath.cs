﻿using UnityEngine;
using System.Collections;

public class WallDeath : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Wall") {
			Destroy(gameObject);
			Application.LoadLevel(Application.loadedLevel);
		}
	}
}
