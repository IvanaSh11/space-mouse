﻿using UnityEngine;
using System.Collections;

public class LaserSpawn : MonoBehaviour {

	public GameObject laser;
	public float speed = 1.3f;

	private int number;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("MakeLaser", 0, 3f);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	}

	void MakeLaser()
	{
		number = (int)Random.Range (0f, 3.9f);
		GameObject current = Instantiate (laser);
		if (number == 0) {
			current.GetComponent<SpriteRenderer> ().color = Color.blue;
			current.tag = "Blue Laser";
		} else if (number == 1) {
			current.GetComponent<SpriteRenderer> ().color = Color.green;
			current.tag = "Green Laser";
		} else if (number == 2) {
			current.GetComponent<SpriteRenderer> ().color = Color.yellow;
			current.tag = "Yellow Laser";
		} else {
			current.GetComponent<SpriteRenderer> ().color = Color.magenta;
			current.tag = "Purple Laser";
		}
		current.transform.position = transform.position;
		current.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-speed, 0);
	}
}
