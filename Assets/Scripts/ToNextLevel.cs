﻿using UnityEngine;
using System.Collections;

public class ToNextLevel : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player") {
			if(Application.loadedLevel==2) Application.LoadLevel(0);
			Application.LoadLevel (Application.loadedLevel +1);
		}
	}
}
