﻿using UnityEngine;
using System.Collections;

public class BulletImpact : MonoBehaviour {



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other)
	{ 
		Color current = GetComponent<SpriteRenderer> ().color;
		if ((other.tag == "Green Creature" && current==Color.green)
		    || (other.tag=="Blue Creature" && current==Color.blue)
		    || (other.tag=="Yellow Creature" && current==Color.yellow)
		    || (other.tag=="Purple Creature" && current==Color.magenta)) {
			Destroy (other.gameObject);
			Destroy(gameObject);
		}

	}
}
