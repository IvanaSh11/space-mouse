﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour {

	public GameObject bullet;
	public PlayerChangeLight lite;

	// Use this for initialization
	void Start () {
		lite = this.transform.parent.GetComponent<PlayerChangeLight> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown (KeyCode.Space))
		{
			GameObject current = Instantiate (bullet);
			if(lite.typeOfLight == 0)
			{
				current.GetComponent<SpriteRenderer>().color = Color.blue;
			}
			else if(lite.typeOfLight ==1)
			{
				current.GetComponent<SpriteRenderer>().color = Color.green;
			}
			else if(lite.typeOfLight==2)
			{
				current.GetComponent<SpriteRenderer>().color = Color.yellow;
			}
			else
			{
				current.GetComponent<SpriteRenderer>().color = Color.magenta;
			}
			current.transform.position = transform.position;
			current.GetComponent<Rigidbody2D>().AddForce (new Vector2(1000f, 0));
		}
	}
}
