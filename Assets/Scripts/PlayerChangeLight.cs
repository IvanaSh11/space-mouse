﻿using UnityEngine;
using System.Collections;

public class PlayerChangeLight : MonoBehaviour {

	Light lite;
	public int typeOfLight;
	public PlayerController ctrl;

	// Use this for initialization
	void Start () {
		lite = GetComponent<Light> ();
		typeOfLight = 0;
		lite.color = Color.cyan;
		ctrl = GetComponent<PlayerController> ();
	}
	
	// Update is called once per frame
	void Update () {
		ColorChange ();
	}

	void ColorChange()
	{
		if (Input.GetButtonDown ("Blue")) {
			typeOfLight = 0;
			lite.color = Color.cyan;
		} else if (Input.GetButtonDown ("Green")) {
			typeOfLight = 1;
			lite.color = Color.green;
		} else if (Input.GetButtonDown ("Yellow")) {
			typeOfLight = 2;
			lite.color = Color.yellow;
		} else if (Input.GetButtonDown ("Purple")) {
			typeOfLight = 3;
			lite.color = Color.magenta;
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if ((other.tag == "Blue Glass" && typeOfLight == 0) ||
			(other.tag == "Green Glass" && typeOfLight == 1) ||
			(other.tag == "Yellow Glass" && typeOfLight == 2) ||
			(other.tag == "Purple Glass" && typeOfLight == 3)) {
			Destroy (other.gameObject);
			ctrl.speed++;
		} else if(other.tag=="Blue Glass" || other.tag=="Green Glass" || other.tag=="Yellow Glass" || other.tag=="Purple Glass"){
			Destroy (gameObject);
			Application.LoadLevel(Application.loadedLevel);
		}
	}
}
