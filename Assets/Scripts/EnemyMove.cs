﻿using UnityEngine;
using System.Collections;

public class EnemyMove : MonoBehaviour {
	public GameObject target;
	Rigidbody2D rBody;
	public float speed = 1f;

	void Awake() {
		rBody = GetComponent<Rigidbody2D>();
		target = GameObject.Find ("Player");
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		rBody.velocity = (target.transform.position - transform.position).normalized * speed;
	}
}
