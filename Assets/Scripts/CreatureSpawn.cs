﻿using UnityEngine;
using System.Collections;

public class CreatureSpawn : MonoBehaviour {

	public GameObject yellow;
	public GameObject purple;
	public GameObject green;
	public GameObject blue;

	private int number;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("MakeCreature", 0, 5f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void MakeCreature()
	{
		number = (int)Random.Range (0f, 3.9f);
		if (number == 0)
			Instantiate (blue, this.transform.position, this.transform.rotation);
		else if (number == 1)
			Instantiate (green, this.transform.position, this.transform.rotation);
		else if (number == 2)
			Instantiate (yellow, this.transform.position, this.transform.rotation);
		else
			Instantiate (purple, this.transform.position, this.transform.rotation);
	}
}
