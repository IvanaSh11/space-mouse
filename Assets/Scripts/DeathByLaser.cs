﻿using UnityEngine;
using System.Collections;

public class DeathByLaser : MonoBehaviour {

	private PlayerChangeLight lite;

	// Use this for initialization
	void Start () {
		lite = GetComponent<PlayerChangeLight> ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if ((other.tag == "Blue Laser" && lite.typeOfLight == 0) ||
			(other.tag == "Green Laser" && lite.typeOfLight == 1) ||
			(other.tag == "Yellow Laser" && lite.typeOfLight == 2) ||
			(other.tag == "Purple Laser" && lite.typeOfLight == 3))
			Destroy (other.gameObject);
		else if (other.tag == "Blue Laser" || other.tag == "Yellow Laser" || other.tag == "Green Laser" || other.tag == "Purple Laser") {
			Destroy (gameObject);
			Application.LoadLevel(Application.loadedLevel);
		}
	}
}
