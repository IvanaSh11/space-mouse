﻿using UnityEngine;
using System.Collections;

public class CameraSmooth : MonoBehaviour {
	public float dampen = 0.15f;
	private Vector3 velocity;
	public Transform target;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(target!=null)
		{
			/*Vector3 point = Camera.main.WorldToViewportPoint(target.position);
			Vector3 delta = target.position - Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));
			Vector3 destination = transform.position + delta;
			transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampen);*/
			transform.position = new Vector3(target.position.x+2f, this.transform.position.y, this.transform.position.z);
		}
	}
}
